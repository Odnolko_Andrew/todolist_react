import React, { Component } from "react";

class Task extends Component {
  constructor(props) {
    super();
    this.state = {
      task: props.task
    };
  }
  _deleteTask(e) {}
  _changeIsDoneTask(e) {
    this.forceUpdate();
  }
  render() {
    return (
      <div className={this.state.task.isDone ? "task done" : "task"}>
        <input type="checkbox" onClick={this._changeIsDoneTask.bind(this)} />
        {this.state.task.title}
        <span className="delete" onClick={this._deleteTask.bind(this)}>
          X
        </span>
      </div>
    );
  }
}

export default Task;
