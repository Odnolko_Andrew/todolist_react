import React, { Component } from "react";
import "./todolist.css";
import Task from "./Task";

class Todolist extends Component {
  constructor() {
    super();
    this.state = {
      tasks: []
    };
  }

  _addNewTaskClickBtn(e) {
    if (e.key === "Enter") {
      this.setState({
        tasks: [
          ...this.state.tasks,
          { title: e.currentTarget.value, isDone: false }
        ]
      });
      e.currentTarget.value = "";
    }
  }

  _deleteTask(task, e) {}

  render() {
    return (
      <div className="todolist_wrapper">
        <div className="header">
          <input type="text" onKeyPress={this._addNewTaskClickBtn.bind(this)} />
        </div>
        <div className="tasks" />
        {this.state.tasks.map((task, i) => {
          return (
            <li key={i} className="li_wrapper_task">
              <Task task={task} />
            </li>
          );
        })}
      </div>
    );
  }
}

export default Todolist;
